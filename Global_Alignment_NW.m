% Algorithm to the global alingment based on the Needleman–Wunsch algorithm.

close all 
clear all 

% Alternative sequences to make the easiest alingment.  
% a = 'CGTAAA';
% b = 'AAACGT';

% Selection for the sequences in fasta format. 
[files]=uigetfile({'*.fasta;','All Fasta Files'},'Select the Files', 'Multiselect', 'on');
file1a = files{1,1};
file2a = strrep(file1a,'fasta', 'txt'); 

% The files has to be in the main directory of Matlab. 
copyfile(file1a,file2a)
fidp = fopen(file2a); 
Cab = textscan(fidp, '%s', 'delimiter', '\n');
identificator = Cab{1}{1}; 
sequence = strcat(Cab{1}{2:size(Cab{1,1}, 1),:});  
sa = struct('Identificator', identificator, 'Sequence', sequence);
a = sa.Sequence; 


file1b = files{1,2}; 
file2b = strrep(file1b,'fasta', 'txt'); 
copyfile(file1b,file2b)
fidpb = fopen(file2b); 
Cab2 = textscan(fidpb, '%s', 'delimiter', '\n');
identificator2 = Cab2{1}{1}; 
sequence2 = strcat(Cab2{1}{2:size(Cab2{1,1}, 1),:});  
sa2 = struct('Identificator', identificator2, 'Sequence', sequence2);
b = sa2.Sequence; 

% Addition of the empty spaces for the sequences at the beginning. 
A = [' ']; 
Aa = [A a];
Ab = [A b];

% % Portion to make the two sequences equal. 
LAa = length(Aa); 
LAb = length(Ab); 

% Ask for the parameters of the scoring. 
s1 = input('Specify the score when ai = bi:  '); 
s2 = input('Specify the score when ai != bi:  '); 
d = input('Specify the value for the gap penalty:  '); 

% Declaration for the scores for each of the situations that can occur.
F = zeros(LAa, LAb); 
F(1,1) = 0; 
tic
for i = 2:LAa
    F(i,1) = d*(i-1); 
    for j = 2:LAb
        F(1,j) = d*(j-1);
    end 
end 
toc 

tic 
for k = 2:LAa
    for h = 2:LAb
          match = F(k-1,h-1) + ScoreDetermination(Aa(k), Ab(h), s1, s2); 
          delete = F(k-1,h) + d; 
          insert = F(k,h-1) + d;
          F(k,h) = max(max(match, delete), insert);
    end 
end 
toc

imagesc(F)
xlabel('Sequence 1'); 
ylabel('Sequence 2'); 
title('Scoring Matrix F'); 
colorbar
saveas(gcf, 'ScoreMap.png')

%%  Determination of the best aligment according to the previous scores. 

AliA = ''; 
AliB = ''; 
i = length(F(:,1)); 
j = length(F(1,:));
sum = 0;
tic 
while (i > 0 || j > 0)
    if (j == 1 || i == 1)
           break 
    end 

% Verification to include the first terms of the sequences without
% error for 0 index. 
        
    if (j == 1 && i > j)
        AliA = strcat(a(i-1), AliA); 
        AliB = strcat('-',AliB);
        sum = sum + d; 
        break 

    elseif (i == 1 && j > i)
             AliA = strcat('-', AliA); 
             AliB = strcat(b(j-1),AliB); 
             sum = sum + d; 
             break 
             
    elseif (j == 1 && i == 1)
        if (a(i) == b(j))
            AliA = strcat(a(i), AliA); 
            AliB = strcat(b(j),AliB);
            sum = sum + s1; 
        else 
           AliA = strcat(a(i), AliA); 
           AliB = strcat('-',AliB);
           sum = sum + d; 
           break 
        end 
    end 
            
    % Extra conditions of the traceback if the indexes are different of 1. 
    if (i > 0 && j > 0 && F(i,j) == F(i-1,j-1) + ScoreDetermination(a(i-1), b(j-1), s1, s2))
        AliA = strcat(a(i-1),AliA);
        AliB = strcat(b(j-1), AliB);
        sum = sum + F(i,j); 
        i = i - 1; 
        j  = j -1; 
        
    elseif (i > 0 && F(i,j) == F(i-1,j) + d)
            AliA = strcat(a(i-1), AliA); 
            AliB = strcat('-',AliB); 
            sum = sum + d;
            i = i - 1; 
        
     else 
            AliA = strcat('-',AliA); 
            AliB = strcat(b(j-1),AliB); 
            j = j -1; 
            sum = sum + d; 

    end 
    
end 
toc

% Print the alingment in the command window. 
fprintf('The best alignment is:  \n')
fprintf('%s\n',AliA)
fprintf('%s\n',AliB)

% Determination for the identity and gaps. 
iden = 0;
gaps = 0; 
maxlen = max(length(AliA), length(AliB));
for i = 1:maxlen
        if (AliA(i)  == AliB(i))
            iden = iden + 1;
            i = i +1; 
        elseif (AliA(i) == '-' || AliB (i) == '-')
            gaps = gaps + 1; 
            i = i +1; 
        end 
end 

% Creation for the correct fasta file with the general information. 
titles = {'# 1:  ','# 2:  ', '# Mode: Distance', '# Match: ', '# Mismatch: ', '# Gap: ', '# Score: ', '# Length: ', '# Identity: ', '# Gaps: ', '# Alignment 1 is: ', '# Alignment 2 is: '};
elem = {a,b,' ', int2str(s1),int2str(s2), int2str(d), int2str(sum), int2str(LAa), int2str(iden), int2str(gaps), AliA, AliB};
fid = fopen('BestAlignment.fasta','w');
for row = 1:length(titles)
    fprintf(fid, '%s ',titles{row});
    fprintf(fid, '%s\n', elem{row});
end 
fclose(fid); 