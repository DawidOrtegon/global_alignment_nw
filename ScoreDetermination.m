% function to set the score between the basis. 
function score = ScoreDetermination(a,b,s1, s2)
La = length(a); 
Lb = length(b); 

for i = 1:La
    for j = 1:Lb
        if eq(a(i), b(j)) == 1
            score = s1; 
        else 
            score = s2; 
        end 
    end 
end 

end 
